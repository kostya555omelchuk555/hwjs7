function filterBy(arr, dataType) {
  return arr.filter(item => typeof item !== dataType);
}

let arr = ['привіт', 'світ', 23, '23', null];
let filteredArr = filterBy(arr, 'string');
console.log(filteredArr);